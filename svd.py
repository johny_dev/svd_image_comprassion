import matplotlib.pyplot as plt
import numpy as np
import time

from PIL import Image
import sys


def print_image(img):
    plt.figure(figsize=(9, 6))
    plt.imshow(img, cmap='gray')
    plt.show()

def load_image(uri):
    try:
        return Image.open(uri)
    except:
        print("Image file not found")
        sys.exit()


def convert_to_grayscale(img):
    return img.convert('LA')

def get_image_matrix(img):
    n_img = np.array(list(img.getdata(band=0)), float) # convert to numpy array
    n_img.shape = (img.size[1], img.size[0]) # Reshape according to orginal image dimensions
    n_img = np.matrix(n_img) # matrix
    
    return n_img

def get_matrix_svd_products(matrix):
    """
    Returns the SVD product of a matrix in the form of U, S, V
    """
    return np.linalg.svd(matrix) 

def matrix_dimensions(matrix):
    return matrix.shape

def reconstruct_image(matrix_tuple, rank):
    """
    Compressing image 
    """
    U, S, V = matrix_tuple
    return np.matrix(U[:, :rank]) * np.diag(S[:rank]) * np.matrix(V[:rank, :])


def compration_ratio(m_times_n, rank):
    """
    Calculate compration ratio
    """
    m, n = m_times_n
    reconstructed_slots = rank * (m + n + 1)
    ratio = 100 - int(round((reconstructed_slots / (m*n)) * 100))

    return "compration ratio: {ratio} %".format(ratio=ratio)


def svd_copress(image_uri, rank):
    img = load_image(image_uri)
    img = convert_to_grayscale(img)

    print_image(img) # Initial gray image

    img_matrix = get_image_matrix(img)
    print(img_matrix) 
    print("IMAGE dimensions")
    print(matrix_dimensions(img_matrix))
    print("************")

    img_products = U, S, V = get_matrix_svd_products(img_matrix) # Get SVD product, decompose a tuple

    print("U dimensions")
    print(matrix_dimensions(U))
    print("************")

    print("S dimensions")
    print(matrix_dimensions(S))
    print("************")

    print("V dimensions")
    print(matrix_dimensions(V))
    print("************")

    compressed = reconstruct_image(img_products, rank)
    print_image(compressed)

    print(compration_ratio(matrix_dimensions(img_matrix), rank))

if __name__ == "__main__":

    ranks = [10, 30, 50, 100]

    for rank in ranks:
        svd_copress("svd.jpg", rank) ### Compress in different ranks

